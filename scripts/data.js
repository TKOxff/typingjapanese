var test_step = 0;

var test_sentence = [
  "あいうえお",
  "かきくけこ",
  "さしすせそ",
  "たちつてと",
  "なにぬねの",
  "はひふへほ",
  "まみむめも",
  "やゆよ",
  "らりるれろ",
  "わをん",
];

function injectSentence() {
  $("#sentence").empty();

  var line = test_sentence[test_step];
  test_step += 1;
  if (test_step == test_sentence.length) {
    test_step = 0;
  }

  for (var i = 0; i < line.length; i++) {
    var child_span = "<span>" + line[i] + "</span>";
    $("#sentence").append(child_span);
  }

  // console.log($("#sentence").html());

  letters = [];
  $("#sentence span").each(function (index) {
    letters[index] = $(this);
  });
  // console.log(letters);

  $("#user_input").val("");
}

function playSound(color) {
  var filename = "sounds/" + color + ".mp3";
  var sound = new Audio(filename);
  sound.play();
}

var keysound_flip = true;

function playKeypressSound() {
  // keysound_flip = !keysound_flip;
  if (keysound_flip) {
    playSound("keypress0");
  } else {
    playSound("keypress1");
  }
}
