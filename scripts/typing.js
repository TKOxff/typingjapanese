
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function restartSentence() {
  // playSound("keypress1");
  await sleep(500);

  $("#user_input").val("");
  last_letter_index = 0;
  injectSentence();
}

function isAlphabet(str) {
  return /^[a-zA-Z]$/.test(str);
}

function handleKeyUpEvent(event) {
  // console.log("keydown key:" + event.key + " code:" + event.code);

  playKeypressSound();

  var isBackspace = false;
  if (event.key == "Backspace") {
    isBackspace = true;

    if (last_letter_index >= 0) {
      //   console.log("remove return idx:" + last_letter_index);

      letters[last_letter_index].removeClass("good");
      letters[last_letter_index].removeClass("bad");
      last_letter_index -= 1;
    }
    return;
  }

  var user_typing = $("#user_input").val();
  var last_letter = "";
  if (user_typing.length > 0) {
    last_letter = user_typing[user_typing.length - 1];
  }
  if (isAlphabet(last_letter)) {
    // console.log("last letter is alphabet!");
    return;
  }

  // console.log("user_typing: " + user_typing + " last:" + last_letter);

  for (var i = 0; i < user_typing.length; i++) {
    // if (i >= 0 && i < letters.length) {
    if (user_typing[i] == letters[i].text()) {
      // console.log("good = " + user_typing[i])
      if (!letters[i].hasClass("good")) {
        letters[i].removeClass("bad");
        letters[i].addClass("good");
      }
    } else {
      //   console.log("miss = " + user_typing[i] + " full: " + user_typing);
      if (!letters[i].hasClass("bad")) {
        letters[i].removeClass("good");
        letters[i].addClass("bad");
      }
    }

    last_letter_index = i;
  }
  // }
}

function handleCompositionEnd(event) {
  if ($("#user_input").val().length >= letters.length) {
    restartSentence();
  }
}
